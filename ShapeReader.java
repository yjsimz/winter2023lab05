import java.nio.file.*;
import java.util.*;

public class ShapeReader {
    public static void main(String[] args) throws Exception {
		Cone cone = new Cone(3,4);
		System.out.println(cone);
		Cone[] cones = loadTriangles("cones.csv");
        printCones(cones);
    }

    public static Cone[] loadTriangles(String path) throws Exception {
		List<String> linesAsList = Files.readAllLines(Paths.get(path));
        String[] lines = linesAsList.toArray(new String[0]);

        Cone[] cones = new Cone[lines.length];
        for (int i = 0; i < lines.length; i++) {
            String[] pieces = lines[i].split(",");
			double d1 = Double.parseDouble(pieces[0]);  
			double d2 = Double.parseDouble(pieces[1]);  
            cones[i] = new Cone(d1, d2);
        }

        return cones;
    }

    public static void printCones(Cone[] cones) {
        for (Cone cone : cones) {
            System.out.println(cone);
        }
    }
}
